import os 
import shutil
import numpy as np 
import subprocess
import re
import sys
import glob
import traceback
from datetime import datetime
from time import mktime

cwd = os.getcwd()


def FindNewestFile(DirectoryList):
   folderCounter = 0
   listLength = len(DirectoryList)
   TimesOfCreation = np.zeros([listLength,1])
   for folder in DirectoryList:
      dateStr = folder.rsplit("analysis_",1)
      # print(dateStr)
      # print(folder)
      TimeOfCreationDateTime = datetime.strptime(dateStr[1],"%Y-%m-%d-%H-%M-%S-%f")
      TimesOfCreation[folderCounter] = mktime(TimeOfCreationDateTime.timetuple())
      folderCounter = folderCounter +1

   idxNewestFolder = np.argmax(TimesOfCreation)

   return idxNewestFolder


SourceFile = 'postprocessing.inp'
for ii in range(22):
   CrackFolder = "Angle"+str(ii+1)
   if CrackFolder in CurrentDirectoryList:
      TempPath = CurrentPath + "/" + CrackFolder + "/" + "loading"
      tempDirectoryList = glob.glob(TempPath + "/" + "analysis*" )
      # now we have to convert the string afterwards to date time, then basically take the newest one
      # analysis_2019-07-16-11-07-58-847

      if len(tempDirectoryList) == 0:
         print("crack"+str(ii+1)+ "Has no analysis folders" )
      else:
         newestFolderIdx = FindNewestFile(tempDirectoryList)
         FolderName = tempDirectoryList[newestFolderIdx]
         # generate newest path 

         print(FolderName)
         print(" ")

         FinalPath = FolderName + "/" + "postprocessing"

         DestinationFile = FinalPath + '/' + 'postprocessing.inp'
         shutil.copy(SourceFile,DestinationFile) # this is what i needed to happen 

         % submit the postprocess fem 
         subprocess.run("cd "+FinalPath + "; PostprocessFem.exe ; cd " + cwd,shell = True, check = True)




   else:
      print("crack" + str(ii) + "does not exist")