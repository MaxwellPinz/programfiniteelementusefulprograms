# this needs to bue used with CopyInputFilesAndMakeFolders_local.py


file0 = open('ReadInCoeffs.txt','r')
Cstr = file0.readline()
Cstr = Cstr.replace('\n', '')
Dstr = file0.readline()
Dstr = Dstr.replace('\n', '')
DestinationFile = file0.readline()
DestinationFile = DestinationFile.replace('\n', '')
TemplateFile = file0.readline()
print(TemplateFile)
TemplateFile =TemplateFile.replace('\n', '')
file0.close


file1 = open(TemplateFile, 'r') 
file2 = open(DestinationFile ,'w')


#numberC = 50.0
#numberD = 1


for line in file1:
	# if statment 
	#if line.contains('backstress constant C'):
	if "asymptotic softening" in line:
		HeaderString =  '   \'asymptotic softening\'                   '
		#NumStr = str(numberC)
		Numstr = Cstr
		TailingString = 'e8'
		LineEnding = '\n'
		writeStr = HeaderString + Numstr + TailingString + '      ' + Numstr+TailingString+LineEnding
		file2.write(writeStr)

	elif "timescale softening" in line:
		HeaderString = '   \'timescale softening\'                   '
		#NumStr = str(numberD)
		Numstr = Dstr
		TailingString = '0'
		LineEnding = '\n'
		writeStr = HeaderString + Numstr + TailingString + '      ' + Numstr+TailingString+LineEnding
		file2.write(writeStr)

	else:
		file2.write(line)



file1.close
file2.close