import subprocess

def runPreprocessingAndSubmit():

#    directoriesCrack = ['crack1', 'crack3', 'crack4', 'crack5', 'crack6', 'crack7', 'crack8', 
#                         'crack10', 'crack11', 'crack12', 'crack13', 'crack14', 'crack15', 'crack16', 
#                         'crack19', 'crack20', 'crack21', 'crack22', 
#                         'C2crack1', 'C2crack2', 'C2crack5', 'C2crack6', 'C2crack7', 'C2crack8', 
#                         'C2crack9', 'C2crack10', 'C2crack12']    


   # directoriesCrack = ['crack1','crack4', 'crack5', 'crack6', 'crack7',  'crack13', 'crack14', 'crack22', 
   #                      'C2crack1', 'C2crack2', 'C2crack6', 'C2crack8', 
   #                      'C2crack9', 'C2crack10']    


   directoriesCrack = ['crack1'] 


   for directoryCrack in directoriesCrack:
      subprocess.run("cd " + directoryCrack + "; mkdir preprocessing;cd ..; python MovePatchFiles.py",shell = True, check = True)


   for directoryCrack in directoriesCrack:
      subprocess.run("cd " + directoryCrack + "; mkdir loading; mv {ABQ_crack.inp,ANS_crack.inp,propertiesFeature.out} preprocessing; cd preprocessing; cp ../../preprocessing.inp .; cp ../../PatchConstructor.m .; ~/cmrl/bin/ConvertMeshToFem.exe preprocessing.inp; cd ..; cp ../preprocessForFem.py .; cp ../RunJob.scr loading; cp ../*.inp .; python preprocessForFem.py; cd loading; qsub RunJob.scr; cd ../../", 
                                                                        shell = True, check = True)

runPreprocessingAndSubmit()
