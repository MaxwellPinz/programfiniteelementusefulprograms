import numpy as np
import os
import subprocess
import shutil

MaxLoading = 1300e6
nCycles = 200
# open file
TimeIncrementationFile = "input/1300BcsNeuman50Cyc.inp"
file1 = open( TimeIncrementationFile ,'w')
# write header
# 1
#    1 transient
#       'number of time points' 76
#       'number of cycles' 1
#       'warm-up time' 0.0

# calculate the number of time points
nTimePts = 4*nCycles+2
file1.write(str(1)+" \n")
file1.write("    1 transient" +" \n")
file1.write("       'number of time points' "+ str(nTimePts) +" \n")
file1.write("       'number of cycles' 1" + " \n")
file1.write("       'warm-up time' 0.0" + " \n")

# for loop for nCycles
for ii in range(nCycles):
  numStartStr = str(4*ii)
  numPosStr = str(4*ii+1)
  numNeutralStr = str(4*ii+2)
  numNegStr = str(4*ii+3)

  file1.write("       " + numStartStr   +"      " + str(0) +" \n")
  file1.write("       " + numPosStr     +"      " + str(MaxLoading) +" \n")
  file1.write("       " + numNeutralStr +"      " + str(0) +" \n")
  file1.write("       " + numNegStr     +"      " + str(-MaxLoading) +" \n")

# return to the final neutral state
file1.write("       " + str(nCycles*4)   +"      " + str(0) +" \n")
file1.write("       " + str(nCycles*4+1)   +"      " + str(MaxLoading) +" \n")

file1.close()