import numpy as np
import os
import subprocess
import shutil



#Cvalues = [0.001,0.1,1,10,500]

#Cvalues = [1,50,500,5000]

#Dvalues = [0.001,0.01,0.1,1]
Dvalues = [1,0.1,0.01]

FileHeader = "SingleCrystalStress"
FileTemplate = "input"

D_Header = "_D_"

C_Header = "_C_"


C = 1

#for C in Cvalues:
for D in Dvalues:    
    # copy all of the things, call the other python program 
    Dstr = str(D)
    Cstr = str(C)
    File = FileHeader+D_Header+Dstr+C_Header+Cstr
    #try:
    #	os.mkdir(File)
    #except:
    #	print('Ihavetodosomething')	
    #subprocess.run("cp -r " + FileTemplate + " " + File+'/')# copys some stuff 
    if os.path.exists(File):
    	shutil.rmtree(File)
    shutil.copytree(FileTemplate, File)


    # now we want to run the python program
    # make a file called something 
    InputCoeffsFile = "ReadInCoeffs.txt"




    file2 = open( InputCoeffsFile ,'w')
    DestinationFile = File+'/materialProperties.inp'
    SourceFile = 'materialProperties.inp'
    file2.write(Cstr+'\n')
    file2.write(Dstr+'\n')
    file2.write(DestinationFile+'\n')
    file2.write(SourceFile+'\n')
    file2.close()

    os.system('python ChangeCoefficents.py')

    #subprocess.run("cd " + File +" ; MainFiniteElementCmrl.exe & ; cd ../", shell = True) 
    #currentPath=os.getcwd()
    #os.system('pwd')
    #print(File)
    #subprocess.run('cd ' + currentPath + File, shell = True) 
    #subprocess.run('ls', shell = True)
    #subprocess.run('cd ' + currentPath + File, shell = True) 
    os.system("cd " + File +" ; MainFiniteElementCmrl.exe & ")
    #os.system('ls')
    #os.system('cd '+File)
    #os.system('MainFiniteElementCmrl.exe &')
    #os.system('cd ../')
    # run the python code 


