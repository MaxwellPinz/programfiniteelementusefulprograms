import os 
import shutil
import numpy as np 
import subprocess
import re
import sys
import glob 
import traceback
from datetime import datetime
from time import mktime

cwd = os.getcwd()


tempDirectoryList = glob.glob(cwd + "/" + "FreeSubmit*" )

SourceFile = 'runPostProcessing.py'

print(tempDirectoryList)

for iDriectory in tempDirectoryList:
   DestFile = iDriectory + '/' + SourceFile
   shutil.copy(SourceFile,DestFile)

   shutil.copy('postprocessing.inp',iDriectory + '/' + 'postprocessing.inp')

   try:
      subprocess.run("cd " + iDriectory + "; python runPostProcessing.py ; cd " + cwd , shell = True, check = True)
   except:
      pass   