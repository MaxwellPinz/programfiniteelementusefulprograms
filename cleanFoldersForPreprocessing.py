# clean directory and ready to re-run preprocessing
# step 1 is to copy the 3 needed files 

import shutil
import os
CurrentDirectoryList = os.listdir()

copyFileList = ['ANS_crack.inp','ABQ_crack.inp','propertiesFeature.out']

AcceptableFolderNames = []

for ii in range(22):
   AcceptableFolderNames.append('crack'+ str(ii+1)) # the +1 is becasuse python is stupid and likes to index on 0 
for ii in range(12):
   AcceptableFolderNames.append('C2crack'+ str(ii+1)) # the +1 is becasuse python is stupid and likes to index on 0 



#print(AcceptableFolderNames)


for iName in CurrentDirectoryList: 
   if iName in AcceptableFolderNames:
      for iFile in copyFileList: 
         source =  iName + '/' + 'preprocessing' + '/' + iFile  
         #print(source)
         destination = iName + '/' + iFile
         try:
            shutil.copyfile(source,destination)
         except:
            pass
            #print(iName + ' Failure')  


#now we need to remove all of the old folders 

for iName in CurrentDirectoryList:
   if iName in AcceptableFolderNames:
      InFolderdirectoryList = os.listdir(iName)
      for jName in InFolderdirectoryList:
         if jName in copyFileList:
            # do nothing 
            pass
         else:
            #shutil.rmtree(iName + '/' + jName)
            if os.path.isfile(iName + '/' + jName):
               os.remove(iName + '/' + jName)
            elif os.path.isdir(iName + '/' + jName):
               shutil.rmtree(iName + '/' + jName)
            

