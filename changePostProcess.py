# this should be written in python 
# step one is to navigate to the folders as necessary 
#then we have to modif the postprocessing.inp 

# Most of what i need should be written in the change inputs thing i wrote a bit ago. 
# just wrap it in a directory finder and error catcher. 

import shutil
import os
import glob
from datetime import datetime
from time import mktime
import numpy as np

CurrentDirectoryList = os.listdir()
CurrentPath = os.getcwd()

# I need to find all of the files that i should cd into 

oldList = (' "component"  "plastic slip" ',' "component"  "plastic defect energy" ')
replacmentList = (' "sum"  "plastic slip" ',' "sum"  "plastic defect energy" ')


print(oldList)


def FindNewestFile(DirectoryList):
   folderCounter = 0
   listLength = len(DirectoryList)
   TimesOfCreation = np.zeros([listLength,1])
   for folder in DirectoryList:
      dateStr = folder.rsplit("analysis_",1)
      # print(dateStr)
      # print(folder)
      TimeOfCreationDateTime = datetime.strptime(dateStr[1],"%Y-%m-%d-%H-%M-%S-%f")
      TimesOfCreation[folderCounter] = mktime(TimeOfCreationDateTime.timetuple())
      folderCounter = folderCounter +1

   idxNewestFolder = np.argmax(TimesOfCreation)

   return idxNewestFolder



def keywordReplacer(ReferenceFile,DestinationFile):
   file1 = open(ReferenceFile, 'r') 
   file2 = open(DestinationFile ,'w')
   # I think the plan for this is to come up with a list of keywords, and also an np array with the replacment values? 
   # open file 
   for line in file1:

      if any(oldListItem in line for oldListItem in oldList): 
      # I wanted to use this, but you are not allowed to access oldListItem outside of this
      # now i have to find which OldListItem is in 
         for oldListItem in oldList:
            if (oldListItem in line):
               oldListItem_save = oldListItem
         print('Replacment Made')
         iReplacment = oldList.index(oldListItem_save)
         writeLine = line.replace(oldListItem_save,replacmentList[iReplacment])
      else:
         writeLine = line
      file2.write(writeLine)
   file1.close
   file2.close






for ii in range(22):
   CrackFolder = "crack"+str(ii+1)
   if CrackFolder in CurrentDirectoryList:
      TempPath = CurrentPath + "/" + CrackFolder + "/" + "loading"
      tempDirectoryList = glob.glob(TempPath + "/" + "analysis*" )
      # now we have to convert the string afterwards to date time, then basically take the newest one
      # analysis_2019-07-16-11-07-58-847

      if len(tempDirectoryList) == 0:
         print("crack"+str(ii+1)+ "Has no analysis folders" )
      else:
         newestFolderIdx = FindNewestFile(tempDirectoryList)
         FolderName = tempDirectoryList[newestFolderIdx]
         # generate newest path 

         print(FolderName)
         print(" ")

         FinalPath = FolderName + "/" + "postprocessing"


         # reconsider renaming the old postprocessing file to postprocessing old 

         shutil.move(FinalPath + "/" + "postprocessing.inp" , FinalPath + "/" + "postprocessingOld.inp") # rename the old one 

         keywordReplacer(FinalPath+"/"+"postprocessingOld.inp",FinalPath+"/"+"postprocessing.inp")

         # now to call another subfucntion 
         # that one will basically take the postrpcessing.inp file and re-write it 





   else:
      print("crack" + str(ii) + "does not exist")





