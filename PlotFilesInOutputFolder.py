import os 
import matplotlib.pyplot as plt
import numpy as np

directoryListNames = os.listdir()



for item in directoryListNames: 
  if item.startswith('SingleCrystalStress'):
    # we want to go in and perform the plotting of the things 
    localList=os.listdir(item)
    for itemLocal in localList:
      if itemLocal.startswith('analysis'):
        # go into output 
        # volumetric_time.out 
        # volumetric_thermalssdresistance.out
        # volumetric_softeningresistance.out
        # volumetric_accumulatedplasticslip.out
        FilePathHeader = item+'/'+itemLocal+'/output/'
        #print(FilePathHeader+'volumetric_time.out')
        timeVolumetric = np.loadtxt(FilePathHeader+'volumetric_time.out')
        cauchyStress = np.loadtxt(FilePathHeader+'volumetric_cauchystress.out')
        trueStrain = np.loadtxt(FilePathHeader+'volumetric_truestrain.out')
        thermalSsdResistance = np.loadtxt(FilePathHeader+'volumetric_thermalssdresistance.out')
        softeningResistance = np.loadtxt(FilePathHeader+'volumetric_softeningresistance.out')
        accumulatedPlasticSlip = np.loadtxt(FilePathHeader+'volumetric_accumulatedplasticslip.out')
        # now to plot the figures, and save them in the appropriate spots 
        print(FilePathHeader)

        accumulatedPlasticSlip = accumulatedPlasticSlip[:,1:]
        #print(np.shape(accumulatedPlasticSlip))


        Indexes = np.where(accumulatedPlasticSlip == np.amax(accumulatedPlasticSlip))
        plotInd = Indexes[1]+1 # actually the second one
        #print(np.amax(accumulatedPlasticSlip))
        print(plotInd)
      

        #print(softeningResistance.shape)
        #print(timeVolumetric.shape)
        plt.figure
        plt.ylabel('Mpa')
        plt.plot(timeVolumetric/4,thermalSsdResistance[:,plotInd]+softeningResistance[:,plotInd] - thermalSsdResistance[0,plotInd])
        plt.savefig(FilePathHeader+'TotalResistance.png')
        plt.close("all")
        plt.figure
        plt.ylabel('pa')
        plt.plot(timeVolumetric/4,thermalSsdResistance[:,plotInd] - thermalSsdResistance[0,plotInd])
        plt.savefig(FilePathHeader+'Hardening.png')
        plt.close("all")
        plt.figure
        plt.ylabel('pa')
        plt.plot(timeVolumetric/4,softeningResistance[:,plotInd])
        plt.savefig(FilePathHeader+'Softening.png')
        plt.close("all")

        plt.figure
        plt.ylabel('pa')
        plt.xlabel('True Strain')
        plt.plot(trueStrain[:,3],cauchyStress[:,3])
        plt.savefig(FilePathHeader+'StressStrain.png')
        plt.close("all")



        plt.figure
        plt.ylabel('Mpa')
        plt.xlabel('Cycle No')
        plt.plot(timeVolumetric/4,accumulatedPlasticSlip[:,plotInd-1])
        plt.savefig(FilePathHeader+'AccPlasticSlip.png')
        plt.close("all")



