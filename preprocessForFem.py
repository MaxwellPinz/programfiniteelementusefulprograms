
import subprocess

def preprocessForFem():

   subprocess.run("cp inputMain.inp loading/inputMain.inp", 
                                                                        shell = True, check = True)
   subprocess.run("cp options.inp loading/options.inp", 
                                                                        shell = True, check = True)   
   subprocess.run("cp preprocessing/coordinatesNodal.out loading/MESH_coordinatesNodal.inp", 
                                                                        shell = True, check = True)
   subprocess.run("cp preprocessing/connectivitiesElement.out loading/MESH_connectivitiesElement.inp", 
                                                                        shell = True, check = True)
   subprocess.run("cp preprocessing/PATCH.inp loading/MESH_patchesFbar.inp", 
                                                                        shell = True, check = True)                                                                           
   subprocess.run("cp preprocessing/setsNode.out loading/MESH_setsNode.inp", 
                                                                        shell = True, check = True)
   subprocess.run("cp preprocessing/setsElementFeature.out loading/MESH_setsElement.inp", 
                                                                         shell = True, check = True)   
   subprocess.run("cp materialProperties.inp loading/materialProperties.inp", 
                                                                        shell = True, check = True)


   with open('preprocessing/setsElementFeature.out') as fileSetElementFeature,                     \
        open('preprocessing/setsElement.out') as fileSetElement,                                   \
        open('loading/BC_neumann.inp', 'w') as fileBcsNeumann,                                     \
        open('loading/MESH_setsElement.inp', 'w') as fileSetElementCombined:

      fileBcsNeumann.write('4\n')
      shouldKeepWriting = False

      isLineFirst = True
      for line in fileSetElementFeature.readlines():

         if isLineFirst:
            nFeatures = int(line.strip())
            fileSetElementCombined.write(str(nFeatures + 4) + '\n')
            isLineFirst = False
         else:
            fileSetElementCombined.write(line)

      for line in fileSetElement.readlines():
         lineSplit = line.split()

         if shouldKeepWriting and not 'explicit' in line:
            fileSetElementCombined.write(line)

         if lineSplit[0] == '13' and 'explicit' in line:
            shouldKeepWriting = True
            fileSetElementCombined.write(line.replace('13', str(nFeatures + 1)))
            fileBcsNeumann.write('ElementSet ' + str(nFeatures + 1) + ' 1 Explicit .true. 1\n')
            fileBcsNeumann.write('1.0 0.0 0.0\n')

         elif lineSplit[0] == '14' and 'explicit' in line:
            shouldKeepWriting = True
            fileSetElementCombined.write(line.replace('14', str(nFeatures + 2)))
            fileBcsNeumann.write('ElementSet ' + str(nFeatures + 2) + ' 2 Explicit .true. 1\n')
            fileBcsNeumann.write('1.0 0.0 0.0\n')

         elif lineSplit[0] == '15' and 'explicit' in line:
            shouldKeepWriting = True
            fileSetElementCombined.write(line.replace('15', str(nFeatures + 3)))
            fileBcsNeumann.write('ElementSet ' + str(nFeatures + 3) + ' 3 Explicit .true. 1\n')
            fileBcsNeumann.write('1.0 0.0 0.0\n')

         elif lineSplit[0] == '16' and 'explicit' in line:
            shouldKeepWriting = True
            fileSetElementCombined.write(line.replace('16', str(nFeatures + 4)))
            fileBcsNeumann.write('ElementSet ' + str(nFeatures + 4) + ' 4 Explicit .true. 1\n')
            fileBcsNeumann.write('1.0 0.0 0.0\n')

         elif 'explicit' in line:
            shouldKeepWriting = False


   with open('loading/PROP_propertiesFeature.inp', 'w') as filePropertiesFeature:

      isLineFirst = True
      isMaterialFirst = True

      with open('preprocessing/orientationsFeature.out') as fileOrientation,                       \
           open('preprocessing/sizesFeature.out') as fileSize:

         for lineFileOrientation in fileOrientation.readlines():

            if isLineFirst:

               nFeatures = lineFileOrientation.split()[0]
               lineSplitFileSize = fileSize.readline().split()

               filePropertiesFeature.write(nFeatures + '\n')
               isLineFirst = False

            else:

               lineSplitFileOrientation = lineFileOrientation.split()
               lineSplitFileSize = fileSize.readline().split()

               if isMaterialFirst:

                  isMaterialFirst = False
                  filePropertiesFeature.write(
                        lineSplitFileOrientation[0] + ' 5 1 ' 
                        + lineSplitFileOrientation[1] + ' ' + lineSplitFileOrientation[2] + ' '
                        + lineSplitFileOrientation[3] + ' ' + lineSplitFileSize[1] + ' ' + '\n')

               else:

                  filePropertiesFeature.write(
                        lineSplitFileOrientation[0] + ' 5 2 ' 
                        + lineSplitFileOrientation[1] + ' ' + lineSplitFileOrientation[2] + ' '
                        + lineSplitFileOrientation[3] + ' ' + lineSplitFileSize[1] + ' ' + '\n')

      
   with open('loading/MESH_mapSetElementToIdFeature.inp', 'w') as fileMapFeature:

      fileMapFeature.write(nFeatures + '\n')
      for iFeature in range(int(nFeatures)):

         fileMapFeature.write(str(iFeature + 1) + ' ' + str(iFeature + 1) + '\n')

   with open('loading/MESH_coordinatesNodal.inp') as fileCoordinates:

      directionLoading = 1

      isLineFirst = True
      isLineSecond = True
      for line in fileCoordinates.readlines():

         if isLineFirst:

            isLineFirst = False

         elif isLineSecond:

            isLineSecond = False
            coordinateLoading = float(line.split()[directionLoading])
            coordinateMin = coordinateLoading
            coordinateMax = coordinateLoading

         else:

            coordinateLoading = float(line.split()[directionLoading])

            if coordinateLoading < coordinateMin:
               coordinateMin = coordinateLoading
            if coordinateLoading > coordinateMax:
               coordinateMax = coordinateLoading

   lengthSampleLoading = coordinateMax - coordinateMin

   strainTotal = 0.008
   rateStrain = 1e-3
   nCycles = 15
   timeUnit = (strainTotal / rateStrain)
   nTimePts = nCycles*4+2

   with open('loading/BC_dirichlet.inp', 'w') as fileBcsDirichlet:
      fileBcsDirichlet.write(
             "2\n"
             + "   1  steady\n"
             + "      0.0\n"
             + "   2  transient\n"
             + "      'number of time points' " +str(nTimePts)+ " \n"
             + "      'number of cycles' 1\n"
             + "      'warm-up time' 0.0\n")
      for iCycle in range(nCycles):
        fileBcsDirichlet.write(
               "      " + str(iCycle*timeUnit*4) + "     0.0\n"
             + "      " + str(iCycle*timeUnit*4+1) + " " + str(strainTotal * lengthSampleLoading) + "\n"
             + "      " + str(iCycle*timeUnit*4+2) + "     0.0\n"
             + "      " + str(iCycle*timeUnit*4+3) + " " + str(-strainTotal * lengthSampleLoading) + "\n" )
      fileBcsDirichlet.write(
               "      " + str(nCycles*timeUnit*4) + "     0.0\n"
             + "      " + str(nCycles*timeUnit*4+1) + " " + str(strainTotal * lengthSampleLoading) + "\n")
preprocessForFem()