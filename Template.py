import os 
import shutil
import numpy as np 
import subprocess
import re
import sys
import traceback
from datetime import datetime
from time import mktime

cwd = os.getcwd()
print(cwd + "/programfiniteelementusefulprograms")
sys.path.append(cwd + "/programfiniteelementusefulprograms")
LimitedPostProcessingFileName = 'LimitedPostProcessing.inp'
# import max defined things here 
import MaxDefinedFunctions as mx