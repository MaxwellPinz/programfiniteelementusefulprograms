import os 
import shutil
import numpy as np 
import subprocess
import re
import sys
import traceback
from datetime import datetime
from time import mktime

cwd = os.getcwd()
print(cwd + "/programfiniteelementusefulprograms")
sys.path.append(cwd + "/programfiniteelementusefulprograms")
LimitedPostProcessingFileName = 'LimitedPostProcessing.inp'
# import max defined things here 
import MaxDefinedFunctions as mx


MaterialPropString =  '\'elastic constants\''
nSimulations = 5
Fractions = np.logspace(-0.5,0.5,nSimulations)
IsFraction = True
OccurenceToReWrite = 3
TemplateFolder = 'SurroundedSubmit'


for iSimulation in range(nSimulations):
   DestinationFolder = TemplateFolder + '_' + str(iSimulation+1)
   if os.path.exists(DestinationFolder):
      shutil.rmtree(DestinationFolder)
   shutil.copytree(TemplateFolder,DestinationFolder)
   FileName_MaterialProps_Source = TemplateFolder + '/' + 'materialProperties.inp'
   FileName_MaterialProps_Dest  = DestinationFolder + '/' + 'materialProperties.inp'

   ReplacmentValues = Fractions[iSimulation]
   mx.ChangeMaterialProperties(FileName_MaterialProps_Source,FileName_MaterialProps_Dest,MaterialPropString,ReplacmentValues,IsFraction,OccurenceToReWrite)
   subprocess.run('cd ' + DestinationFolder + '; python runPreprocessingAndSubmit.py ; cd ..' ,shell = True, check = True)