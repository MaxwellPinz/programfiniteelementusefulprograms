# README #

This repository is meant as a place to keep helpful codes related to running ProgramFiniteElementProgramCmrl. This should be composed of mostly matlab and python codes that allow one to set up the pre-preprocessing files, such as changing material parameters for different runs, cleaning a folder so it can be re-preprocessed. etc. 

### What is this repository for? ###

This repository is meant as a place to keep helpful codes related to running ProgramFiniteElementProgramCmrl. This should be composed of mostly matlab and python codes that allow one to set up the pre-preprocessing files, such as changing material parameters for different runs, cleaning a folder so it can be re-preprocessed. etc. 

### How do I get set up? ###

No idea. 

### Contribution guidelines ###

Codes should follow CMRL coding guidelines, (in spirit). 

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact