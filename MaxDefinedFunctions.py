import re
import numpy


# FileName = 'plotsUnDeformedConfiguration_displacement.plt'
# OutputIPFileName = '../../../../CrackOutputData.mat'
# OutputNodalFileName = '../../../../CrackNodalOutputData.mat'



def FindIndexesOfSubstringsInStr(SubStr,Str):
   A = [m.start() for m in re.finditer(SubStr,Str)]
   return(A)


def FindStateVarPrecentile(Precentile,FileName):
   file1 = open(FileName, 'r') 

   line1 = file1.readline()
   StringLoc = FindIndexesOfSubstringsInStr('"',line1)
   print(len(StringLoc))
   nStrings = int(len(StringLoc)/2)


   TitleStrings = []
   for istring in range(nStrings):
     upper = int(istring*2+1)
     lower = int(istring*2)
     TitleStrings = TitleStrings + [line1[(StringLoc[lower]+1):StringLoc[upper]]]

   nStateVars = nStrings - 3

   line2 = file1.readline()


   SeperatedSecondLine = line2.rsplit(',')


   # here we define nodes and elements 
   eval(SeperatedSecondLine[1]) # finds the number of nodes 
   eval(SeperatedSecondLine[2]) # finds the number of elements 
   nNodes = nodes
   nElements = elements


   # the next step is to take all of the data and pack it into one long unbroken list of stuff
   ReadData =[]
   for line in file1: # read rest of lines
      ReadData.append([float(x) for x in line.split()])

   # okay we have read in the rest of the data 

   nodalLocationData = np.zeros(nNodes,3)
   IntegrationPointData = np.zeros(nElements,nStateVars);
   Connectivites = np.zeros(4,nElements) # I dont strictly need this but it might make sense to have this lying around for future 

   LinearCounter = 0
   for iCol in range(3):
      nodalLocationData[:,iCol] = ReadData[LinearCounter:(LinearCounter + nNodes -1)];
      LinearCounter = LinearCounter + nNodes; 

   for iStateVar in range(nStateVars):
      IntegrationPointData[:,iStateVar] = ReadData[LinearCounter:(LinearCounter + nElements -1)]
      LinearCounter = LinearCounter + nElements ; 



   # for the sake of brevity, since in know im only using one state variable 
   SortedPlasticStrain = np.sort(IntegrationPointData[:,1])
   # find the 99th precentile. 
   round(nElements*(1-Precentile))


def ChangeMaterialProperties(FileName_MaterialProps_Source,FileName_MaterialProps_Dest,MaterialPropList,ReplacmentValues,IsFraction):
   # Usage
   # FileName_MaterialProps_Source,FileName_MaterialProps_Dest should be seperate file names 
   # MaterialPropString is for example '\'elastic constants\''
   # IsFraction = True, Multiplies everything by what is in ReplacmentValues 
   # Here Replacment Vaules should have length = 1
   # for Is Fraction = false, then the values in ReplamentValues 
   # ReplacmentValues does double duty 

   file_Source = open(FileName_MaterialProps_Source, 'r') 
   file_Dest = open(FileName_MaterialProps_Dest,'w')

   for line in file_Source:
    HasWritten = False
    iMatProp = -1
    for MaterialPropString in MaterialPropList: 
      iMatProp = iMatProp +1
      if MaterialPropString in line:
        #print(line)
        line_removedMaterialProp = line.replace(MaterialPropString,' ')
        #print(line_removedMaterialProp)
        #line_removedMaterialProp = line.remove(MaterialPropString)
        Line_split= line_removedMaterialProp.split()
        # the first one should be the material prop string, followed by the numbers
        #print(Line_split)
        writeString =  '   ' + MaterialPropString + '                  '
        if IsFraction:             
          #print(Line_split)   
          for iValue in Line_split:
            FormattedValue = iValue.replace('d','e')

            #writeString = writeString + (str(float(fixedValue)*ReplacmentValues)+'')
            AdjustedValue = float(FormattedValue)*ReplacmentValues[iMatProp]
            replacmentStr = "{:e}".format(AdjustedValue)
            writeString = writeString + (replacmentStr+'  ')

        else:
          for iValue in ReplacmentValues:
            writeString = writeString + (str(iValue)+'  ')

        file_Dest.write(writeString + '\n')
        HasWritten = True


    if HasWritten == False:
      HasWritten = True
      file_Dest.write(line)


# def ChangeMaterialProperties(FileName_MaterialProps_Source,FileName_MaterialProps_Dest,MaterialPropString,ReplacmentValues,IsFraction,OccurenceToReWrite):
#    # Usage
#    # FileName_MaterialProps_Source,FileName_MaterialProps_Dest should be seperate file names 
#    # MaterialPropString is for example '\'elastic constants\''
#    # IsFraction = True, Multiplies everything by what is in ReplacmentValues 
#    # Here Replacment Vaules should have length = 1
#    # for Is Fraction = false, then the values in ReplamentValues 
#    # ReplacmentValues does double duty 


#    file_Source = open(FileName_MaterialProps_Source, 'r') 
#    file_Dest = open(FileName_MaterialProps_Dest,'w')



#    Occurence = 0
#    for line in file_Source:
#     if MaterialPropString in line:
#       Occurence = Occurence +1
#       if Occurence >= OccurenceToReWrite:
#         line_removedMaterialProp = line.replace(MaterialPropString,'')
#         Line_split= line_removedMaterialProp.split()
#         # the first one should be the material prop string, followed by the numbers
#         #print(Line_split)
#         writeString =  '   ' + MaterialPropString + '                  '
#         if IsFraction:                
#           for iValue in Line_split:
#             writeString = writeString + (str(float(iValue)*ReplacmentValues)+'  ')

#         else:
#           for iValue in ReplacmentValues:
#             writeString = writeString + (str(iValue)+'  ')

#         file_Dest.write(writeString + '\n')

#       else: 
#         file_Dest.write(line)

#     else:
#           file_Dest.write(line)



