import os 
import shutil
import numpy as np 
import subprocess
import re
import sys
import traceback
from datetime import datetime
from time import mktime

cwd = os.getcwd()
print(cwd + "/programfiniteelementusefulprograms")
sys.path.append(cwd + "/programfiniteelementusefulprograms")
LimitedPostProcessingFileName = 'LimitedPostProcessing.inp'
# import max defined things here 
import MaxDefinedFunctions as mx


def ZeroPaddedInt2Str(N,DesiredLength):
   lengthInt = len(str(N))
   Zeros2Pad = DesiredLength-lengthInt
   OutputStr = str(N)
   for i in range(Zeros2Pad):
      OutputStr = '0'+OutputStr
   return(OutputStr)


def FindNewestKeywordFile(DirectoryList,KeywordRemove):
   folderCounter = 0
   listLength = len(DirectoryList)
   TimesOfCreation = np.zeros([listLength,1])
   for folder in DirectoryList:
      folderCounter = folderCounter +1
      if KeywordRemove  in folder:
         print("working")
         dateStr = folder.rsplit(KeywordRemove,1)
         # print(dateStr)
         # print(folder)
         TimeOfCreationDateTime = datetime.strptime(dateStr[1],"%Y-%m-%d-%H-%M-%S-%f")
         TimesOfCreation[folderCounter-1] = mktime(TimeOfCreationDateTime.timetuple())
         

   print(TimesOfCreation)
   #print(DirectoryList)
   idxNewestFolder = np.argmax(TimesOfCreation)
   FolderName = DirectoryList[idxNewestFolder]

   return FolderName



# find all files that end with QT

# pour one out for avicci
Levels = [50,100,150,200,250,300,350,400,450,500,550]

#Levels = [50]

nLevels = int(len(Levels))
#print(nLevels)

PlasticSlipSave = np.zeros([nLevels,1])



TargetMaxStrain = .04


SimPathHeader = 'CrackNucleationC10_'
SimPathTrailing = '_QT'

levelCounter = 0
for level in Levels:
   levelCounter = levelCounter +1
   try:
      SimulationPath = SimPathHeader + ZeroPaddedInt2Str(level,3)+SimPathTrailing

      loadingFolder = cwd +'/'+ SimulationPath + '/crack1'+'/loading'
      loadingDirectoryList = os.listdir(loadingFolder)
      # find the newest analsys folder 
      AnalsysFolderName = FindNewestKeywordFile(loadingDirectoryList,"analysis_")
      # at this point the options are either to use the postprocessing code and make a newest po
      postprocessingFolderPath = loadingFolder +"/"+ AnalsysFolderName + '/postprocessing' 

      #copy this file 
      shutil.copyfile(LimitedPostProcessingFileName,postprocessingFolderPath + '/' + 'postprocessing.inp')
      # do postprocessing with the limited postprocessing file 
      subprocess.run("cd " + postprocessingFolderPath + " ; " + "PostprocessFem.exe " + " ; "  + "cd " + cwd,shell = True, check = True) 

      postprocessingDirectoryList = os.listdir(postprocessingFolderPath)
      ProcessFolder = FindNewestKeywordFile(postprocessingDirectoryList,'process_')

      # now we have the postprocessing file 

      # lets pretend this calls the find max plastic strain 
      Precentile = 0.999
      PLTFileName =  postprocessingFolderPath + '/' + ProcessFolder + '/' + 'plotsUnDeformedConfiguration_displacement.plt'
      PlasticSlipSave[levelCounter] =  mx.FindStateVarPrecentile(Precentile,PLTFileName)
      
   except:
      print(str(levelCounter+1) + 'hasFailed')
print(PlasticSlipSave)


MaxPlasticSlipFileName = 'MaxPlasticSlip.txt'

file2 = open(MaxPlasticSlipFileName ,'w')
levelCounter = 0
for level in Levels:
   file2.write(str(level) + ',' + str(PlasticSlipSave[levelCounter]) + '\n')
   levelCounter = levelCounter +1

file2.close